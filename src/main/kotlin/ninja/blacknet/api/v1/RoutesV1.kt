/*
 * Copyright (c) 2018-2019 Pavel Vasin
 * Copyright (c) 2019 Blacknet Team
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.api.v1

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.list
import ninja.blacknet.Config
import ninja.blacknet.api.*
import ninja.blacknet.coding.toHex
import ninja.blacknet.core.*
import ninja.blacknet.crypto.*
import ninja.blacknet.db.*
import ninja.blacknet.network.Network
import ninja.blacknet.network.Node
import ninja.blacknet.serialization.Json
import ninja.blacknet.serialization.SerializableByteArray
import ninja.blacknet.transaction.*
import ninja.blacknet.util.*
import java.io.File
import kotlin.math.abs

fun Route.APIV1() {
    webSocket("/api/v1/notify/block") {
        try {
            APIServer.blockNotifyV0.add(outgoing)
            while (true) {
                incoming.receive()
            }
        } catch (e: ClosedReceiveChannelException) {
        } finally {
            APIServer.blockNotifyV0.remove(outgoing)
        }
    }

    webSocket("/api/v2/notify/block") {
        try {
            APIServer.blockNotifyV1.add(outgoing)
            while (true) {
                incoming.receive()
            }
        } catch (e: ClosedReceiveChannelException) {
        } finally {
            APIServer.blockNotifyV1.remove(outgoing)
        }
    }

    webSocket("/api/v1/notify/transaction") {
        try {
            while (true) {
                val string = (incoming.receive() as Frame.Text).readText()
                val publicKey = Address.decode(string) ?: return@webSocket this.close(CloseReason(CloseReason.Codes.PROTOCOL_ERROR, "invalid account"))

                APIServer.walletNotifyV1.mutex.withLock {
                    val keys = APIServer.walletNotifyV1.map.get(outgoing)
                    if (keys == null) {
                        @Suppress("NAME_SHADOWING")
                        val keys = HashSet<PublicKey>()
                        keys.add(publicKey)
                        APIServer.walletNotifyV1.map.put(outgoing, keys)
                    } else {
                        keys.add(publicKey)
                    }
                }
            }
        } catch (e: ClosedReceiveChannelException) {
        } finally {
            APIServer.walletNotifyV1.remove(outgoing)
        }
    }

    get("/api/v1/peerinfo") {
        call.respond(Json.stringify(PeerInfoV1.serializer().list, PeerInfoV1.getAll()))
    }

    get("/api/v1/nodeinfo") {
        call.respond(Json.stringify(NodeInfo.serializer(), NodeInfo.get()))
    }

    get("/api/v1/peerdb") {
        call.respond(Json.stringify(PeerDBInfo.serializer(), PeerDBInfo.get()))
    }

    get("/api/v1/peerdb/networkstat") {
        call.respond(Json.stringify(PeerDBInfo.serializer(), PeerDBInfo.get(true)))
    }

    get("/api/v1/leveldb/stats") {
        call.respond(LevelDB.getProperty("leveldb.stats") ?: "Not implemented")
    }

    get("/api/v1/blockdb/get/{hash}/{txdetail?}") {
        val hash = Hash.fromString(call.parameters["hash"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid hash")
        val txdetail = call.parameters["txdetail"]?.toBoolean() ?: false

        val result = BlockInfoV1.get(hash, txdetail)
        if (result != null)
            call.respond(Json.stringify(BlockInfoV1.serializer(), result))
        else
            call.respond(HttpStatusCode.NotFound, "block not found")
    }

    get("/api/v2/blockdb/get/{hash}/{txdetail?}") {
        val hash = Hash.fromString(call.parameters["hash"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid hash")
        val txdetail = call.parameters["txdetail"]?.toBoolean() ?: false

        val result = BlockDB.block(hash)
        if (result != null)
            call.respond(Json.stringify(BlockInfoV2.serializer(), BlockInfoV2(result.first, hash, result.second, txdetail)))
        else
            call.respond(HttpStatusCode.NotFound, "block not found")
    }

    get("/api/v1/blockdb/getblockhash/{height}") {
        val height = call.parameters["height"]?.toIntOrNull() ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid height")

        BlockDB.mutex.withLock {
            val state = LedgerDB.state()
            if (height < 0 || height > state.height)
                return@get call.respond(HttpStatusCode.NotFound, "block not found")
            else if (height == 0)
                return@get call.respond(Hash.ZERO.toString())
            else if (height == state.height)
                return@get call.respond(state.blockHash.toString())

            if (APIServer.lastIndex != null && APIServer.lastIndex!!.second.height == height)
                return@get call.respond(APIServer.lastIndex!!.first.toString())

            var hash: Hash
            var index: ChainIndex
            if (height < state.height / 2) {
                hash = Hash.ZERO
                index = LedgerDB.getChainIndex(hash)!!
            } else {
                hash = state.blockHash
                index = LedgerDB.getChainIndex(hash)!!
            }
            if (APIServer.lastIndex != null && abs(height - index.height) > abs(height - APIServer.lastIndex!!.second.height))
                index = APIServer.lastIndex!!.second
            while (index.height > height) {
                hash = index.previous
                index = LedgerDB.getChainIndex(hash)!!
            }
            while (index.height < height) {
                hash = index.next
                index = LedgerDB.getChainIndex(hash)!!
            }
            if (index.height < state.height - PoS.MATURITY + 1)
                APIServer.lastIndex = Pair(hash, index)
            call.respond(hash.toString())
        }
    }

    get("/api/v1/blockdb/getblockindex/{hash}/") {
        val hash = Hash.fromString(call.parameters["hash"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid hash")

        val result = LedgerDB.getChainIndex(hash)
        if (result != null)
            call.respond(Json.stringify(ChainIndex.serializer(), result))
        else
            call.respond(HttpStatusCode.NotFound, "block not found")
    }

    get("/api/v1/blockdb/makebootstrap") {
        val checkpoint = LedgerDB.state().rollingCheckpoint
        if (checkpoint == Hash.ZERO)
            return@get call.respond(HttpStatusCode.BadRequest, "not synchronized")

        val file = File(Config.dataDir, "bootstrap.dat.new")
        val stream = file.outputStream().buffered().data()

        var hash = Hash.ZERO
        var index = LedgerDB.getChainIndex(hash)!!
        do {
            hash = index.next
            index = LedgerDB.getChainIndex(hash)!!
            val bytes = BlockDB.getImpl(hash)!!
            stream.writeInt(bytes.size)
            stream.write(bytes, 0, bytes.size)
        } while (hash != checkpoint)

        stream.close()

        call.respond(file.absolutePath)
    }

    get("/api/v1/ledger") {
        call.respond(Json.stringify(LedgerInfo.serializer(), LedgerInfo.get()))
    }

    get("/api/v1/ledger/get/{account}/{confirmations?}") {
        val publicKey = Address.decode(call.parameters["account"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid account")
        val confirmations = call.parameters["confirmations"]?.toIntOrNull() ?: PoS.DEFAULT_CONFIRMATIONS
        val result = AccountInfoV1.get(publicKey, confirmations)
        if (result != null)
            call.respond(Json.stringify(AccountInfoV1.serializer(), result))
        else
            call.respond(HttpStatusCode.NotFound, "account not found")
    }

    get("/api/v1/ledger/check") {
        call.respond(Json.stringify(LedgerDB.Check.serializer(), LedgerDB.check()))
    }

    get("/api/v1/txpool") {
        call.respond(Json.stringify(TxPoolInfo.serializer(), TxPoolInfo.get()))
    }

    get("/api/v1/account/generate") {
        val wordlist = Wordlists.get("english")!!

        call.respond(Json.stringify(NewMnemonicInfo.serializer(), NewMnemonicInfo.new(wordlist)))
    }

    get("/api/v1/address/info/{address}") {
        val info = AddressInfo.fromString(call.parameters["address"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")

        call.respond(Json.stringify(AddressInfo.serializer(), info))
    }

    post("/api/v1/mnemonic/info/{mnemonic}") {
        val info = NewMnemonicInfo.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Json.stringify(NewMnemonicInfo.serializer(), info))
    }

    post("/api/v1/transfer/{mnemonic}/{fee}/{amount}/{to}/{message?}/{encrypted?}/{blockHash?}/") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val from = privateKey.toPublicKey()
        val fee = call.parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid fee")
        val amount = call.parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid amount")
        val to = Address.decode(call.parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid to")
        val encrypted = call.parameters["encrypted"]?.let { it.toByteOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid encrypted") }
        val message = Message.create(call.parameters["message"], encrypted, privateKey, to) ?: return@post call.respond(HttpStatusCode.BadRequest, "failed to create message")
        val blockHash = call.parameters["blockHash"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid blockHash") }

        APIServer.txMutex.withLock {
            val seq = WalletDB.getSequence(from) ?: return@post call.respond(HttpStatusCode.BadRequest, "wallet reached sequence threshold")
            val data = Transfer(amount, to, message).serialize()
            val tx = Transaction.create(from, seq, blockHash
                    ?: WalletDB.referenceChain(), fee, TxType.Transfer.type, data)
            val signed = tx.sign(privateKey)

            if (Node.broadcastTx(signed.first, signed.second) == Accepted)
                call.respond(signed.first.toString())
            else
                call.respond("Transaction rejected")
        }
    }

    post("/api/v1/burn/{mnemonic}/{fee}/{amount}/{message?}/{blockHash?}/") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val from = privateKey.toPublicKey()
        val fee = call.parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid fee")
        val amount = call.parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid amount")
        val message = SerializableByteArray.fromString(call.parameters["message"].orEmpty()) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid message")
        val blockHash = call.parameters["blockHash"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid blockHash") }

        APIServer.txMutex.withLock {
            val seq = WalletDB.getSequence(from) ?: return@post call.respond(HttpStatusCode.BadRequest, "wallet reached sequence threshold")
            val data = Burn(amount, message).serialize()
            val tx = Transaction.create(from, seq, blockHash ?: WalletDB.referenceChain(), fee, TxType.Burn.type, data)
            val signed = tx.sign(privateKey)

            if (Node.broadcastTx(signed.first, signed.second) == Accepted)
                call.respond(signed.first.toString())
            else
                call.respond("Transaction rejected")
        }
    }

    post("/api/v1/lease/{mnemonic}/{fee}/{amount}/{to}/{blockHash?}/") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val from = privateKey.toPublicKey()
        val fee = call.parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid fee")
        val amount = call.parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid amount")
        val to = Address.decode(call.parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid to")
        val blockHash = call.parameters["blockHash"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid blockHash") }

        APIServer.txMutex.withLock {
            val seq = WalletDB.getSequence(from) ?: return@post call.respond(HttpStatusCode.BadRequest, "wallet reached sequence threshold")
            val data = Lease(amount, to).serialize()
            val tx = Transaction.create(from, seq, blockHash ?: WalletDB.referenceChain(), fee, TxType.Lease.type, data)
            val signed = tx.sign(privateKey)

            if (Node.broadcastTx(signed.first, signed.second) == Accepted)
                call.respond(signed.first.toString())
            else
                call.respond("Transaction rejected")
        }
    }

    post("/api/v1/cancellease/{mnemonic}/{fee}/{amount}/{to}/{height}/{blockHash?}/") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val from = privateKey.toPublicKey()
        val fee = call.parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid fee")
        val amount = call.parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid amount")
        val to = Address.decode(call.parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid to")
        val height = call.parameters["height"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid height")
        val blockHash = call.parameters["blockHash"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid blockHash") }

        APIServer.txMutex.withLock {
            val seq = WalletDB.getSequence(from) ?: return@post call.respond(HttpStatusCode.BadRequest, "wallet reached sequence threshold")
            val data = CancelLease(amount, to, height).serialize()
            val tx = Transaction.create(from, seq, blockHash
                    ?: WalletDB.referenceChain(), fee, TxType.CancelLease.type, data)
            val signed = tx.sign(privateKey)

            if (Node.broadcastTx(signed.first, signed.second) == Accepted)
                call.respond(signed.first.toString())
            else
                call.respond("Transaction rejected")
        }
    }

    get("/api/v1/transaction/raw/send/{serialized}/") {
        val serialized = SerializableByteArray.fromString(call.parameters["serialized"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid serialized")
        val hash = Transaction.hash(serialized.array)

        APIServer.txMutex.withLock {
            if (Node.broadcastTx(hash, serialized.array) == Accepted)
                call.respond(hash.toString())
            else
                call.respond("Transaction rejected")
        }
    }

    post("/api/v1/decryptmessage/{mnemonic}/{from}/{message}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val publicKey = Address.decode(call.parameters["from"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid from")
        val message = call.parameters["message"] ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid message")

        val decrypted = Message.decrypt(privateKey, publicKey, message)
        if (decrypted != null)
            call.respond(decrypted)
        else
            call.respond(HttpStatusCode.NotFound, "Decryption failed")
    }

    post("/api/v1/signmessage/{mnemonic}/{message}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")
        val message = call.parameters["message"] ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid message")

        val signature = Message.sign(privateKey, message)

        call.respond(signature.toString())
    }

    get("/api/v1/verifymessage/{account}/{signature}/{message}") {
        val publicKey = Address.decode(call.parameters["account"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid account")
        val signature = Signature.fromString(call.parameters["signature"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid signature")
        val message = call.parameters["message"] ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid message")

        val result = Message.verify(publicKey, signature, message)

        call.respond(result.toString())
    }

    get("/api/v1/addpeer/{address}/{port?}/{force?}") {
        val port = call.parameters["port"]?.let { Network.parsePort(it) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid port") } ?: Node.DEFAULT_P2P_PORT
        val address = Network.parse(call.parameters["address"], port) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")
        val force = call.parameters["force"]?.toBoolean() ?: false

        try {
            val connection = Node.connections.find { it.remoteAddress == address }
            if (force || connection == null) {
                Node.connectTo(address)
                call.respond("Connected")
            } else {
                call.respond("Already connected on ${connection.localAddress}")
            }
        } catch (e: Throwable) {
            call.respond(e.message ?: "unknown error")
        }
    }

    get("/api/v1/disconnectpeer/{address}/{port?}/{force?}") {
        val port = call.parameters["port"]?.let { Network.parsePort(it) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid port") } ?: Node.DEFAULT_P2P_PORT
        val address = Network.parse(call.parameters["address"], port) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")
        @Suppress("UNUSED_VARIABLE")
        val force = call.parameters["force"]?.toBoolean() ?: false

        val connection = Node.connections.find { it.remoteAddress == address }
        if (connection != null) {
            connection.close()
            call.respond("Disconnected")
        } else {
            call.respond("Not connected to ${address}")
        }
    }

    get("/api/v1/disconnectpeerbyid/{id}/{force?}") {
        val id = call.parameters["id"]?.toLongOrNull() ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid id")
        @Suppress("UNUSED_VARIABLE")
        val force = call.parameters["force"]?.toBoolean() ?: false

        val connection = Node.connections.find { it.peerId == id }
        if (connection != null) {
            connection.close()
            call.respond("Disconnected")
        } else {
            call.respond("Not connected")
        }
    }

    post("/api/v1/staker/start/{mnemonic}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Staker.startStaking(privateKey).toString())
    }

    post("/api/v1/staker/stop/{mnemonic}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Staker.stopStaking(privateKey).toString())
    }

    post("/api/v1/startStaking/{mnemonic}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Staker.startStaking(privateKey).toString())
    }

    post("/api/v1/stopStaking/{mnemonic}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Staker.stopStaking(privateKey).toString())
    }

    post("/api/v1/isStaking/{mnemonic}") {
        val privateKey = Mnemonic.fromString(call.parameters["mnemonic"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "invalid mnemonic")

        call.respond(Staker.isStaking(privateKey).toString())
    }

    get("/api/v1/walletdb/getwallet/{address}") {
        val publicKey = Address.decode(call.parameters["address"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")

        WalletDB.mutex.withLock {
            call.respond(Json.stringify(WalletDB.WalletV1.serializer(), WalletDB.getWalletImpl(publicKey).toV1()))
        }
    }

    get("/api/v1/walletdb/getoutleases/{address}") {
        val publicKey = Address.decode(call.parameters["address"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")

        WalletDB.mutex.withLock {
            val wallet = WalletDB.getWalletImpl(publicKey)
            call.respond(Json.stringify(AccountState.Lease.serializer().list, wallet.outLeases))
        }
    }

    get("/api/v1/walletdb/getsequence/{address}") {
        val publicKey = Address.decode(call.parameters["address"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid address")

        call.respond(WalletDB.getSequence(publicKey).toString())
    }

    get("/api/v1/walletdb/gettransaction/{hash}/{raw?}") {
        val hash = Hash.fromString(call.parameters["hash"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid hash")
        val raw = call.parameters["raw"]?.toBoolean() ?: false

        val result = WalletDB.mutex.withLock {
            WalletDB.getTransactionImpl(hash)
        }
        if (result != null) {
            if (raw)
                return@get call.respond(result.toHex())

            val tx = Transaction.deserialize(result)
            call.respond(Json.stringify(TransactionInfoV2.serializer(), TransactionInfoV2(tx, hash, result.size)))
        } else {
            call.respond(HttpStatusCode.NotFound, "transaction not found")
        }
    }

    get("/api/v1/walletdb/getconfirmations/{hash}") {
        val hash = Hash.fromString(call.parameters["hash"]) ?: return@get call.respond(HttpStatusCode.BadRequest, "invalid hash")

        val result = WalletDB.getConfirmations(hash)
        if (result != null)
            call.respond(result.toString())
        else
            call.respond(HttpStatusCode.NotFound, "transaction not found")
    }
}
